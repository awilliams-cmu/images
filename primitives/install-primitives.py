#!/usr/bin/env python3

import argparse
import json
import os.path
import shlex
import subprocess
import sys
from urllib import parse as url_parse
import re

# To have both stderr and stdout interleaved together.
sys.stderr = sys.stdout

parser = argparse.ArgumentParser(description="Generate script to install primitives from primitive annotations.")
parser.add_argument(
    '-a', '--annotations',
    required=True,
    help="directory with primitive annotations to install",
)
parser.add_argument(
    '--pip-no-binary',
    help="pip packages to install from source rather than binaries",
)
parser.add_argument(
    '--output',
    help="installation script to output",
)
arguments = parser.parse_args()


def get_annotations():
    for dirpath, dirnames, filenames in os.walk(arguments.annotations):
        for filename in filenames:
            if filename != 'primitive.json':
                continue

            annotation_path = os.path.join(dirpath, filename)

            print(">>> Processing '{annotation_path}'.".format(annotation_path=annotation_path), flush=True)

            with open(annotation_path, 'r') as annotation_file:
                yield json.load(annotation_file)


def run(args):
    subprocess.run(args, stdout=sys.stdout, stderr=sys.stdout, check=True, encoding='utf8')
    sys.stdout.flush()


_canonicalize_regex = re.compile(r"[-_.]+")


def pypi_canonicalize(name):
    return _canonicalize_regex.sub('-', name).lower()


apt_requirements = set()
pip_requirements = {}
pip_url_requirements = set()
for annotation in get_annotations():
    try:
        for installation_entry in annotation['installation']:
            if installation_entry['type'] == 'PIP' and 'package' in installation_entry:
                if 'registry' in installation_entry and installation_entry['registry'] != 'https://pypi.python.org/simple':
                    raise ValueError("Alternative registries are no longer supported")
                package = pypi_canonicalize(installation_entry['package'])
                print(">>> Adding Python package '{package}=={version}'.".format(package=package, version=installation_entry['version']), flush=True)
                if package not in pip_requirements:
                    pip_requirements[package] = installation_entry['version']
                elif pip_requirements[package] != installation_entry['version']:
                    raise ValueError("Incompatible requirements: {package}=={version1},=={version2}".format(
                        package=package,
                        version1=pip_requirements[package],
                        version2=installation_entry['version'],
                    ))

            elif installation_entry['type'] == 'PIP' and 'package_uri' in installation_entry:
                print(">>> Adding Python package '{package_uri}'.".format(package_uri=installation_entry['package_uri']), flush=True)
                # git+git scheme is not supported, and other URIs can be parsed with urlparse.
                parsed_uri = url_parse.urlparse(installation_entry['package_uri'])

                # Not a git URI. Just directly install, without "--editable" argument.
                if not parsed_uri.scheme.startswith('git'):
                    package_uri = url_parse.urlunparse(parsed_uri)
                    pip_url_requirements.add((False, package_uri))

                else:
                    # To install with "--editable" argument an "egg" fragment argument has to be provided.
                    # We parse it here and set it to Python path to assure it is unique, if it does not yet exist.
                    # We require it during primitive annotation validation in primitives index repository, but
                    # we still want to be robust here.
                    if parsed_uri.fragment:
                        parsed_fragment = url_parse.parse_qs(parsed_uri.fragment, strict_parsing=True)
                    else:
                        parsed_fragment = {}

                    if 'egg' not in parsed_fragment:
                        parsed_fragment['egg'] = [annotation['python_path']]

                    parsed_uri = parsed_uri._replace(fragment=url_parse.urlencode(parsed_fragment, doseq=True, safe='/', quote_via=url_parse.quote))

                    package_uri = url_parse.urlunparse(parsed_uri)

                    pip_url_requirements.add((True, package_uri))

            elif installation_entry['type'] == 'UBUNTU':
                print(">>> Adding Ubuntu package '{package}'.".format(package=installation_entry['package']), flush=True)
                apt_requirements.add(installation_entry['package'])

    except Exception as error:
        if arguments.annotations:
            raise
        elif os.environ['D3M_INTERFACE_VERSION'] == 'devel':
            print(">>> Exception during processing but ignoring because it is a devel image: {error}".format(error=error), flush=True)
        else:
            raise


with open(arguments.output, 'w') as output:
    output.write("#!/bin/sh\nset -e\n")
    if apt_requirements:
        output.write("apt-get update -q -q\n")
        output.write("apt-get install --yes --force-yes --no-install-recommends")
        for package in apt_requirements:
            output.write(" \\\n    {package}".format(package=package))
        output.write("\n")

    if pip_requirements or pip_url_requirements:
        output.write("pip3 --disable-pip-version-check install --upgrade --upgrade-strategy only-if-needed")
        if arguments.pip_no_binary:
            output.write(" --no-binary " + arguments.pip_no_binary)
        for package, version in pip_requirements.items():
            output.write(" \\\n    {package}=={version}".format(package=package, version=version))
        for editable, url in pip_url_requirements:
            output.write(" \\\n    ")
            if editable:
                output.write("--editable ")
            output.write(shlex.quote(url))
        output.write("\n")

    output.write("echo \">>> All available primitives:\"\n")
    output.write("python3 -m d3m index search\n")
