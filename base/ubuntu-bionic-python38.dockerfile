FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

# apt-utils seems missing and warnings are shown, so we install it.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-utils tzdata locales file sudo && \
 echo 'UTC' > /etc/timezone && \
 rm /etc/localtime && \
 dpkg-reconfigure tzdata && \
 apt-get upgrade --yes --force-yes && \
 rm -f /etc/cron.weekly/fstrim && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Prevent Python packages to be installed through APT.
COPY ./etc/apt/preferences.d/no-python2 /etc/apt/preferences.d/no-python2

# Prevent Python 3.6 packages to be installed through APT.
COPY ./etc/apt/preferences.d/no-python36 /etc/apt/preferences.d/no-python36

# General dependencies for building dependencies, locales, and some utilities.
RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends swig git build-essential cmake wget ssh tar gzip ca-certificates unzip curl libyaml-dev libssl-dev equivs vim-tiny jq && \
 locale-gen --no-purge en_US.UTF-8 && \
 update-locale LANG=en_US.UTF-8 && \
 echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections && \
 echo locales locales/default_environment_locale select en_US.UTF-8 | debconf-set-selections && \
 dpkg-reconfigure locales && \
 update-alternatives --install /usr/bin/vim vim /usr/bin/vim.tiny 10 && \
 curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
 apt-get install --yes --force-yes git-lfs && \
 git lfs install && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Set locale to UTF-8 which makes Python read text in UTF-8 and not ASCII, by default.
ENV LC_ALL=en_US.UTF-8

# Enable Python's fault handler
# https://docs.python.org/3/library/faulthandler.html
ENV PYTHONFAULTHANDLER=1

# Installing Python 3.8.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes python3.8 python3.8-dev && \
 update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 10 && \
 update-alternatives --install /usr/bin/python3-config python3-config /usr/bin/python3.8-config 10 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dummy packages to satisfy unnecessary dependencies on Python 3.6.
COPY ./equivs/python3 /tmp/python3
COPY ./equivs/python3-dev /tmp/python3-dev
RUN cd /tmp && \
 equivs-build python3 && \
 dpkg -i python3-dummy_1.0_all.deb && \
 equivs-build python3-dev && \
 dpkg -i python3-dev-dummy_1.0_all.deb && \
 cd / && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes dh-python python3-distutils && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Installing pip 21.3.1.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes python3-pip python3-openssl && \
 pip3 install pip==21.3.1 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Base Python packages (those dependencies from d3m core package which are needed also in testing and testing-lite images).
# We install PyYAML from source so that a fast C implementation is compiled as well.
RUN pip3 install docker[tls]==5.0.3 && \
 pip3 install --upgrade 'setuptools==59.5.0' && \
 pip3 install frozendict==1.2 && \
 pip3 install --no-binary :all: PyYAML==5.1.2 && \
 pip3 install 'requests>=2.19.1,<=2.23.0' && \
 pip3 install Cython==0.29.24 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Extra Python dependencies.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends zlib1g-dev libexpat1-dev tk8.6-dev libffi-dev libssl-dev \
 libbz2-dev liblzma-dev libncurses5-dev libreadline6-dev libsqlite3-dev libgdbm-dev liblzma-dev lzma lzma-dev \
 gfortran libopenblas-dev liblapack-dev && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dummy package to satisfy unnecessary dependency on Python 2.
# See: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=891712
COPY ./equivs/python-dev /tmp/python-dev
RUN cd /tmp && \
 equivs-build python-dev && \
 dpkg -i python-dev-dummy_1.0_all.deb && \
 cd / && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Compile Git from source, to work around freeze in git-lfs
RUN curl -Lo /tmp/git.tar.gz https://github.com/git/git/archive/refs/tags/v2.35.1.tar.gz && \
 mkdir -p /src/git && \
 tar --strip-components=1 -zxf /tmp/git.tar.gz -C /src/git && \
 cd /src/git && \
 apt-get update -q -q && \
 apt-get install libcurl4-openssl-dev && \
 autoreconf && ./configure --prefix=/usr/local && \
 make -j4 && make install

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
