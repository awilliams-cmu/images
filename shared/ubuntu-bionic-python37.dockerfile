FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python37

# Install CUDA 10.1.
# Based on:
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.1/base/Dockerfile
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.1/devel/Dockerfile
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.1/runtime/Dockerfile
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tools/dockerfiles/dockerfiles/gpu.Dockerfile
ENV CUDA_VERSION 10.1.168
ENV CUDNN_VERSION=7.6.4.38-1
ENV NCCL_VERSION=2.4.2
ENV CUDA_PKG_VERSION=10-1=$CUDA_VERSION-1
RUN \
 curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/3bf863cc.pub | apt-key add - && \
 curl -fsSL https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/7fa2af80.pub | apt-key add - && \
 echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list && \
 echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends \
  cuda-cudart-$CUDA_PKG_VERSION \
  cuda-compat-10-1 \
  cuda-command-line-tools-$CUDA_PKG_VERSION \
  cuda-nvrtc-$CUDA_PKG_VERSION \
  cuda-cufft-$CUDA_PKG_VERSION \
  cuda-curand-$CUDA_PKG_VERSION \
  cuda-cusolver-$CUDA_PKG_VERSION \
  cuda-cusparse-$CUDA_PKG_VERSION \
  cuda-cudart-$CUDA_PKG_VERSION \
  cuda-libraries-$CUDA_PKG_VERSION \
  cuda-nvtx-$CUDA_PKG_VERSION \
  libcudnn7=$CUDNN_VERSION+cuda10.1 \
  libcublas10=10.2.1.243-1 \
  libfreetype6-dev \
  libhdf5-serial-dev \
  libzmq3-dev \
  pkg-config \
  software-properties-common \
  cuda-minimal-build-$CUDA_PKG_VERSION \
  cuda-core-$CUDA_PKG_VERSION \
  libnccl2=$NCCL_VERSION-1+cuda10.1 \
  cuda-cupti-$CUDA_PKG_VERSION \
  cuda-cudart-dev-$CUDA_PKG_VERSION \
  cuda-cusolver-$CUDA_PKG_VERSION && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends libnvinfer6=6.0.1-1+cuda10.1 \
  libnvinfer-plugin6=6.0.1-1+cuda10.1 && \
 ln -s cuda-10.1 /usr/local/cuda && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

RUN \
 echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
 echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf && \
 echo "/usr/local/cuda/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 echo "/usr/local/cuda-10.1/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 echo "/usr/local/cuda/extras/CUPTI/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 ldconfig

# Make sure that we can run also without GPU.
RUN \
 ln -s /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/stubs/libcuda.so.1 && \
 echo "/usr/local/cuda/lib64/stubs" >> /etc/ld.so.conf.d/zzz-cuda-stubs.conf && \
 ldconfig

ENV PATH=/usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}

ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility
ENV NVIDIA_REQUIRE_CUDA=cuda>=10.1 brand=tesla,driver>=384,driver<385 brand=tesla,driver>=396,driver<397 brand=tesla,driver>=410,driver<411

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest

